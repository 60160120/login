var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
require('./connect_database')

var usersRouter = require('./routes/users')
var productsRouter = require('./routes/products')
var customersRouter = require('./routes/customers')

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/users', usersRouter)
app.use('/products', productsRouter)
app.use('/customers', customersRouter)
module.exports = app
