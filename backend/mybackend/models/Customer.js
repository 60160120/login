const mongoose = require('mongoose')
const Schema = mongoose.Schema
const customerSchema = new Schema({
  name: String,
  address: String,
  dob: Date,
  gender: String,
  mobile: String,
  email: String
})

module.exports = mongoose.model('customer', customerSchema)
